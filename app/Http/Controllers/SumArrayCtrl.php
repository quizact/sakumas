<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Models\Menu;
use DB;

class SumArrayCtrl extends Controller
{
    public function index($a,$b)
    {
        $f = explode(",",$b);
        sort($f);
        $c = 0;
        for($i=0;$i<$a;$i++){
            $c+=(int)$f[$i];
        }

        rsort($f);
        $d=0;
        for($i=0;$i<$a;$i++){
            $d+=(int)$f[$i];
        }

        return response()->json([
            'terkecil' => $c,
            'terbesar' => $d
        ]);
    }

    public function kelipatan($m,$n)
    {
        $a = [];
        for($i = $m; $i<=$n;$i++){
            if($i%5 == 0 && $i%7 != 0){
                $a[] =  'saku';
            }else if($i%7 == 0 && $i%5 !=0){
                $a[] = 'mas';
            }else if($i%5 == 0 && $i%7 == 0){
                $a[] = 'sakumas';
            }else{
                $a[] = (int)$i;
            }
        }
        return $a;
    }

    public function list()
    {
        $list = Menu::join('kategori', 'menu.kategori_id', '=', 'kategori.id')->get();
        $menu = Menu::query();
        $semua = $menu->join('kategori', 'menu.kategori_id', '=', 'kategori.id')->selectRaw("kategori.nama as name, count(kategori.nama) as y")->groupBy("kategori.nama")->get()->map(function($item){
            $semua['kategori'] = $item->name;
            $semua['jumlah'] = (float) $item->y;
            return $semua;
        });
        $satu =  collect(DB::table('menu')->select('nama', 'kategori_id', DB::raw('SUM(kategori_id) as jumlah'))->groupBy("nama","kategori_id")->get());
        $filtered = $satu->filter(function ($value, $key) {
            return $value->jumlah == 1;
        });

        $data = array(
            'menu' => $list,
            'menu setiap kategori' => $semua,
            'menu filter' => $filtered
        );
        return $data;
    }
}
