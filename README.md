Requirement:

1. composer
2. mysql
3. npm dan node js
4. php versi 8 ke atas

Cara Install:

1. Jalankan perintah git clone https://gitlab.com/quizact/sakumas di direktory mana saja
2. masuk ke directory sakumas dengan cara ketikkan cd sakumas
3. jalankan perintah composer install
4. jalankan perintah npm install && npm run dev
5. copy database yang ada di direktory public/database ke phpmyadmin atau software tools mysql   lain yang anda punya
6. jalankan perintah php artisan serve
7. url untuk jawaban no 1 adalah localhost:8000/sumarray/{a}/{b}
8. url untuk jawaban no 2 adalah localhost:8000/kelipatan/{m}/{n}
9. url untuk jawaban no 3 adalah localhost:8000/list
10. jawaban yang paling sulit dan paling lama jawab nya adalah yang bagian 3.3
